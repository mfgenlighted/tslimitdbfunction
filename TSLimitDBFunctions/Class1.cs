﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace LimitDBFunctionsDLL
{
    public class LimitFileFunctions
    {
        public struct DatabaseInfo
        {
            public string user;
            public string password;
            public string server;
            public string database;
        }

        private DatabaseInfo databaseInfo = new DatabaseInfo();
        private MySqlConnection connection = null;
        private string mysqlConnectString = string.Empty;
        private int basesystemErrorCode = 0;
        private int basefunctionErrorCode = 0;

        private const int syserror_DatabaseOpenError = 0;


        public LimitFileFunctions(string server, string database, string user, string password, int baseSystemErrorCode, int baseFunctionErrorCode)
        {
            databaseInfo.user = user;
            databaseInfo.password = password;
            databaseInfo.server = server;
            databaseInfo.database = database;
            mysqlConnectString = "Server=" + server + ";Database=" + database + ";uid=" + user + ";password=" + password + ";";
            connection = new MySqlConnection(mysqlConnectString);
        }


        public bool CheckForDatabase(out bool errorOccured, out int errorCode, out string errorMsg)
        {
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                errorMsg = "Error opening " + databaseInfo.database + " on server " + databaseInfo.server + ". " + ex.Message;
                errorCode = basesystemErrorCode - syserror_DatabaseOpenError;
                errorOccured = true;
                return false;
            }
            connection.Close();
            errorMsg = string.Empty;
            errorCode = 0;
            errorOccured = false;
            return true;
        }

        /// <summary>
        /// Read the limits database and select a part number for the sequence file name passed.
        /// </summary>
        /// <param name="dbInfo">DatabaseInfo structure with Limits database sign in infomation</param>
        /// <param name="seqenceName"></param>
        /// <param name="partNumber"></param>
        /// <param name="errorOccurred"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMsg"></param>
        public void SelectPartNumber(string seqenceName, out string partNumber, out string description,
                                        out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            List<string> pnList = new List<string>();
            string pn;

            if (!CheckForDatabase(out errorOccurred, out errorCode, out errorMsg))
            {
                partNumber = string.Empty;
                description = string.Empty;
                return;
            }

            connection.Open();
            cmd = connection.CreateCommand();
            cmd.CommandText = "select DISTINCT(G.NAME), G.DESCRIPTION FROM GROUP_DESC G INNER JOIN PROPERTIES P ON G.GROUP_ID = P.GROUP_ID where P.SEQUENCE_FILE_NAME = '" + seqenceName + "'";
            rdr = cmd.ExecuteReader();
            while (rdr.Read())     // if found some part numbers
            {
                pn = rdr.GetString(0);
                if (pn.Length == 11)      // if the part number(xx-xxxxx-xx) is excatly 11 characters
                    pnList.Add(pn + " : " + rdr.GetString(1));                  // then add it to the list
            }
            if (pnList.Count == 0)      // if no part numbers found
            {
                partNumber = string.Empty;
                description = string.Empty;
                errorOccurred = false;
                errorMsg = string.Empty;
                errorCode = 0;
            }
            else        // at least one part number was found
            {
                if (pnList.Count == 1)      // if only one part number was found
                {
                    partNumber = pnList[0].Substring(0,11);     // get only the part number part
                    description = pnList[0].Substring(pnList[0].IndexOf(':') + 2);
                    errorOccurred = false;
                    errorMsg = string.Empty;
                    errorCode = 0;
                }
                else            // more than one. Have the operator pick one.
                {
                    PickPartNumber picker = new PickPartNumber(pnList);
                    picker.ShowDialog();
                    partNumber = picker.SelectedPN.Substring(0, 11);
                    description = picker.SelectedPN.Substring(picker.SelectedPN.IndexOf(':') + 2);
                    picker.Dispose();
                }
            }
            connection.Close();
            return;
        }
    }
}
