﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LimitDBFunctionsDLL
{
    public partial class PickPartNumber : Form
    {
        List<string> list = new List<string>();
        public string SelectedPN;

        public PickPartNumber(List<string> pnlist)
        {
            InitializeComponent();

            list = pnlist;
            listBox1.DataSource = list;
        }

        private void Select_button_Click(object sender, EventArgs e)
        {
            SelectedPN = listBox1.SelectedItem.ToString();
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
